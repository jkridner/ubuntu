FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install -y apt-utils && \
    apt-get clean
RUN apt-get update && \
    apt-get install -y \
        python3-full \
        python3-dev \
        build-essential \
        libxml2-dev \
        libxslt-dev \
        git \
        graphviz \
        devscripts \
    && \
    apt-get clean
